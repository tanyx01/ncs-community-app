## Note -
This repository is a public mirror of the [original private repository](https://github.com/OjusWiZard/NCS-Community-App) on github.
We are planning to open it publicly soon.
### Why is the original repository private?
Because, it is a college-wide project and we are preparing a surprise for the college fellows.
### Why is it publicly open here?
It is expected that no one will know about this repository in the college. But, we can still share this code to whoever wants to see it outside the campus.

# NCS-Community-App
This is basically an app that connects all the Nibblites to one platform
